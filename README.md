## qssi-user 13 TKQ1.220807.001 00WW_3_310 release-keys
- Manufacturer: hmd global
- Platform: holi
- Codename: PHR_sprout
- Brand: Nokia
- Flavor: qssi-user
- Release Version: 13
- Kernel Version: 5.4.210
- Id: TKQ1.220807.001
- Incremental: 00WW_3_310
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Treble Device: en-US
- Locale: undefined
- Screen Density: Nokia/Punisher_00WW/PHR_sprout:13/TKQ1.220807.001/00WW_3_310:user/release-keys
- Fingerprint: 
- OTA version: qssi-user-13-TKQ1.220807.001-00WW_3_310-release-keys
- Branch: nokia/PHR_sprout
- Repo: 
