#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_PHR_sprout.mk

COMMON_LUNCH_CHOICES := \
    lineage_PHR_sprout-user \
    lineage_PHR_sprout-userdebug \
    lineage_PHR_sprout-eng
