#
# Copyright (C) 2023 The Android Open Source Project
# Copyright (C) 2023 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Omni stuff.
$(call inherit-product, vendor/omni/config/common.mk)

# Inherit from PHR_sprout device
$(call inherit-product, device/hmd/PHR_sprout/device.mk)

PRODUCT_DEVICE := PHR_sprout
PRODUCT_NAME := omni_PHR_sprout
PRODUCT_BRAND := Nokia
PRODUCT_MODEL := Nokia G50
PRODUCT_MANUFACTURER := hmd

PRODUCT_GMS_CLIENTID_BASE := android-hmd

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="Punisher_00WW-user 13 TKQ1.220807.001 00WW_3_310 release-keys"

BUILD_FINGERPRINT := Nokia/Punisher_00WW/PHR_sprout:13/TKQ1.220807.001/00WW_3_310:user/release-keys
